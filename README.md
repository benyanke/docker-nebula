# Docker Nebula

Docker image for Slack's Nebula mesh network agent.

## Description
This image contains a full install of Nebula, for container networking.

## Usage examples

_This section still being written_

### Config

The default command of the container runs the config at `/config/config.yaml`. For normal operation, mount
your nebula configuration at that path, and then run without overriding the `cmd` or `entrypoint.


## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-nebula:latest`
 - `registry.gitlab.com/benyanke/docker-nebula:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-nebula:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-nebula:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer. Generally, you'll want to match the tag to the
nebula version being build.
