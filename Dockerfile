FROM ubuntu:24.04
MAINTAINER Ben Yanke "ben@benyanke.com"

ENV NEBULA_VERSION 1.9.5
ENV NEBULA_ARCHIVE nebula-linux-amd64.tar.gz
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /tmp

# Install Deps
RUN apt-get update -y \
      && apt-get install -y \
            wget \
      && rm -rf /var/lib/apt/lists/*

# Install Nebula
RUN wget -q https://github.com/slackhq/nebula/releases/download/v$NEBULA_VERSION/$NEBULA_ARCHIVE && \
    tar -xvf $NEBULA_ARCHIVE && \
    rm -rf $NEBULA_ARCHIVE && \
    mkdir -p /usr/local/bin && \
    mv nebula /usr/local/bin && \
    mv nebula-cert /usr/local/bin


WORKDIR /config

ENTRYPOINT [ "/usr/local/bin/nebula" ]
CMD ["-config", "/config/config.yaml"]
